"use strict";

/**
 * @namespace EpicApplication
 */
!(function () {
    /**
 * EpicApplication namespace.
 * @type {Object}
 * @property {Array} imagearray - Array to store image data.
 * @property {Array} typeArray - Array of supported types.
 * @property {Object} dateCache - Cache to store dates for each type.
 * @property {Object} imageCache - Cache to store image data for each type and date.
 * @property {Function} initialize - Initializes the application.
 * @property {Function} updateCache - Updates the date cache with a given type and value.
 * @property {Function} updateImageCache - Updates the image cache with a given type, date, and array of images.
 * @property {string} currentType - Holds the current type globally.
 * @property {string} currentDate - Holds the current date globally.
 */
    const EpicApplication = {
        imagearray: [],
        typeArray: ["natural", "enhanced", "aerosol", "cloud"],
        dateCache: {},
        imageCache: {},
        currentType: null,
        currentDate: null,

        /**
         * Initializes the application by setting up the type select dropdown.
         * @memberof EpicApplication
         * @function
         */
        initialize: function () {
            const select = document.getElementById("type");
            select.innerHTML = '';
            this.typeArray.forEach((type) => {
                this.dateCache[type] = null;
                this.imageCache[type] = new Map();
                const option = document.createElement('option');
                option.text = type;
                option.value = type;
                select.appendChild(option);
            });
        },

        /**
         * Updates the date cache with a given type and value.
         * @memberof EpicApplication
         * @function
         * @param {string} type - The type to update the cache for.
         * @param {string} value - The value to set in the cache.
         */
        updateCache: function (type, value) {
            if (this.typeArray.includes(type)) {
                this.dateCache[type] = value;
            }
        },

        /**
         * Updates the image cache with a given type, date, and array of images.
         * @memberof EpicApplication
         * @function
         * @param {string} type - The type to update the cache for.
         * @param {string} date - The date to update the cache for.
         */
        updateImageCache: function (type, date) {
            this.imageCache[type].set(date, this.imagearray);
        },
    }



    document.addEventListener("DOMContentLoaded", e => Restrictdates(e))
    document.addEventListener("DOMContentLoaded", function (e) {

        EpicApplication.initialize();
        const ul = document.getElementById("image-menu");
        let type = document.getElementById("type");
        let date = document.getElementById("date");

        type.addEventListener("change", (e) => Restrictdates(e));

        document.getElementById("request_form").addEventListener("submit", (e) => submit(e, type, date, ul));
        ul.addEventListener("click", (e) => displayImage(e, EpicApplication.imagearray));

    });

    /**
     * Handles the form submission.
     * @memberof EpicApplication
     * @function
     * @param {Event} e - The submit event.
     * @param {HTMLElement} type - The type element.
     * @param {string} date - The date value.
     * @param {HTMLElement} ul - The ul element.
     */
    function submit(e, type, date, ul) {

        e.preventDefault();
        ul.textContent = undefined;
        type = document.getElementById("type").value;
        date = document.getElementById("date").value;
        
        const existingArray = EpicApplication.imageCache[type].has(date) ? EpicApplication.imageCache[type].get(date) :-5;

        if (existingArray === -5) {
            EpicApplication.imagearray = [];

            fetch(`https://epic.gsfc.nasa.gov/api/${type}/date/${date},{}`)
                .then(response => {
                    if (!response.ok) {
                        displayNoImageFound();
                        throw new Error("Error", { cause: response });
                    }
                    return response.json();
                })
                .then(obj => {
                    ul.textContent = undefined;
                    if (obj.length == 0) {
                        displayNoImageFound();
                    } else {
                        obj.forEach((obj) => {
                            EpicApplication.imagearray.push(obj);
                            makeLis(obj.date);
                        })
                        EpicApplication.updateImageCache(type, date);
                        console.log(EpicApplication.imagearray);
                    }
                })
        } else {
            console.log("No fetching");
            if (EpicApplication.imageCache[type].get(date).length == 0) {
                displayNoImageFound();
            }
            EpicApplication.imagearray = existingArray;
            console.log(EpicApplication.imagearray);
            EpicApplication.imagearray.forEach((item) => {
                makeLis(item.date);
            })
        }
        EpicApplication.currentType = type;
        EpicApplication.currentDate = date;

    }

    /**
     * Creates li elements and appends them to the ul element.
     * @memberof EpicApplication
     * @function
     * @param {string} date - The date value.
     */
    function makeLis(date) {
        const ul = document.getElementById("image-menu");
        const template = document.getElementById("image-menu-item");
        const li = template.content.cloneNode(true);
        const liTitle = li.querySelector("span");
        liTitle.textContent = date;
        ul.appendChild(li);
    }
    /**
     * Displays the selected image and details.
     * @memberof EpicApplication
     * @function
     * @param {Event} e - The click event.
     * @param {Array} obj - The array of image objects.
     */
    function displayImage(e, obj) {
        if (EpicApplication.currentType && EpicApplication.currentDate) {
            const everyli = document.querySelectorAll("li");
            if (e.target.textContent == "No planet images found") {
                return;
            }

            for (let i = 0; i < everyli.length; i++) {
                everyli[i].setAttribute("data-image-list-index", i);
            }
            const img = document.getElementById("earth-image");
            const li = e.target.closest("li");
            const earthdate = document.getElementById("earth-image-date");
            const earthtitle = document.getElementById("earth-image-title");
            let formattedDate = EpicApplication.currentDate.replace(/-/g, "/");
            const dataIndex = li.getAttribute("data-image-list-index");
            console.log(`Type :"${EpicApplication.currentType} date :${formattedDate} img : ${EpicApplication.imagearray[dataIndex].image}`);
            img.src = `https://epic.gsfc.nasa.gov/archive/${EpicApplication.currentType}/${formattedDate}/jpg/${EpicApplication.imagearray[dataIndex].image}.jpg`;
            earthdate.textContent = obj[dataIndex].date;
            earthtitle.textContent = obj[dataIndex].caption;
        }
    }

    /**
     * Displays a message when no planet images are found.
     * @memberof EpicApplication
     * @function
     */
    function displayNoImageFound() {
        const ul = document.getElementById("image-menu");
        const li = document.createElement("li");
        li.textContent = "No planet images found";
        ul.appendChild(li);
    }

    /**
 * Restricts dates based on the selected type.
 * @memberof EpicApplication
 * @function
 */
    function Restrictdates() {
        const type = document.getElementById("type").value;
        const dateinput = document.getElementById("date");
        if (EpicApplication.dateCache[type] == null) {
            fetch(`https://epic.gsfc.nasa.gov/api/${type}/all`)
                .then(response => {
                    if (response.ok) {
                        console.log(`${type}`);
                    } else {
                        console.log("failure dates");
                    }
                    return response.json();
                })
                .then(obj => {
                    let datearray = [];
                    obj.forEach((item) => {
                        const date = item.date.toString();
                        datearray.push(date);
                    })

                    let highestArray = datearray.sort((a, b) => b - a);
                    const highestdate = highestArray[0];
                    EpicApplication.updateCache(type, highestdate);
                    dateinput.max = EpicApplication.dateCache[type];

                })
                .catch(err => {
                    console.error("3)Error:", err);
                });
        } else {
            dateinput.max = EpicApplication.dateCache[type];
        }
    }
}());

// End of the IIFE
